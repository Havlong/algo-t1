from random import randint
from re import fullmatch
from time import time_ns

REGEX_PATTERN = r'\d+'
RANDOM_MIN = 0
RANDOM_MAX = 5_000_000
NANOS_IN_MILLIS = 1_000_000


def fillArray(size):
    return [randint(RANDOM_MIN, RANDOM_MAX) for _ in range(size)]


def sortArray(a):
    n = len(a)
    for i in range(n - 1):
        for j in range(i):
            if a[j] > a[j + 1]:
                t = a[j]
                a[j] = a[j + 1]
                a[j + 1] = t


def isValidInput(user_input):
    return fullmatch(REGEX_PATTERN, user_input)


def measureTime(function, *args):
    start = time_ns()
    function(*args)
    finish = time_ns()
    return (finish - start) // NANOS_IN_MILLIS
