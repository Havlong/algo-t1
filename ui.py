import os

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QFileDialog, QWidget

from algo import isValidInput, sortArray, measureTime
from data import arrayInit, writeFile, readFile

DEFAULT_READ_FILE = os.path.join(os.getcwd(), "source_file.csv")
DEFAULT_WRITE_FILE = os.path.join(os.getcwd(), "answer.csv")


class Ui_ArraySorter(QWidget):
    def setupUi(self, ArraySorter):
        self.readFile = DEFAULT_READ_FILE
        self.writeFile = DEFAULT_WRITE_FILE

        ArraySorter.setObjectName("ArraySorter")
        ArraySorter.resize(490, 340)
        ArraySorter.setEnabled(True)
        ArraySorter.setStyleSheet("background-color: rgb(31, 26, 48)")
        self.centralwidget = QtWidgets.QWidget(ArraySorter)
        self.centralwidget.setObjectName("centralwidget")
        self.title = QtWidgets.QLabel(self.centralwidget)
        self.title.setGeometry(QtCore.QRect(0, 0, 490, 40))
        self.title.setStyleSheet("font: 75 16pt \"MS Shell Dlg 2\";\n"
                                 "color: rgb(255, 255, 255);")
        self.title.setAlignment(QtCore.Qt.AlignCenter)
        self.title.setObjectName("title")
        self.inputPathLabel = QtWidgets.QLabel(self.centralwidget)
        self.inputPathLabel.setGeometry(QtCore.QRect(25, 135, 320, 40))
        self.inputPathLabel.setStyleSheet("color: rgb(255, 255, 255);\n"
                                          "font: 75 10pt \"MS Shell Dlg 2\";\n"
                                          "background-color: rgb(57, 48, 77);")
        self.inputPathLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.inputPathLabel.setObjectName("inputPathLabel")
        self.outputPathLabel = QtWidgets.QLabel(self.centralwidget)
        self.outputPathLabel.setGeometry(QtCore.QRect(25, 205, 320, 40))
        self.outputPathLabel.setStyleSheet("color: rgb(255, 255, 255);\n"
                                           "font: 75 10pt \"MS Shell Dlg 2\";\n"
                                           "background-color: rgb(57, 48, 77);")
        self.outputPathLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.outputPathLabel.setObjectName("outputPathLabel")
        self.sortTimeLabel = QtWidgets.QLabel(self.centralwidget)
        self.sortTimeLabel.setGeometry(QtCore.QRect(25, 275, 320, 40))
        self.sortTimeLabel.setStyleSheet("color: rgb(255, 255, 255);\n"
                                         "font: 75 10pt \"MS Shell Dlg 2\";\n"
                                         "background-color: rgb(57, 48, 77);")
        self.sortTimeLabel.setText("")
        self.sortTimeLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.sortTimeLabel.setObjectName("sortTimeLabel")
        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit.setGeometry(QtCore.QRect(25, 65, 320, 40))
        self.lineEdit.setStyleSheet("background-color: rgb(57, 48, 77);\n"
                                    "font: 10pt \"MS Shell Dlg 2\";\n"
                                    "color: rgb(255, 255, 255);\n"
                                    "border: 1px solid rgb(57, 48, 77);\n"
                                    "")
        self.lineEdit.setInputMask("")
        self.lineEdit.setText("")
        self.lineEdit.setAlignment(QtCore.Qt.AlignCenter)
        self.lineEdit.setPlaceholderText("")
        self.lineEdit.setObjectName("lineEdit")
        self.changeInputBtn = QtWidgets.QPushButton(self.centralwidget)
        self.changeInputBtn.setGeometry(QtCore.QRect(365, 135, 100, 40))
        self.changeInputBtn.setStyleSheet("color: rgb(255, 255, 255);\n"
                                          "background-color: rgb(13, 246, 227);\n"
                                          "color: rgb(0, 0, 0);\n"
                                          "font: 75 italic 10pt \"Noto Sans\";\n"
                                          "border: 1px solid rgb(31, 26, 48);\n"
                                          "border-radius: 10px;")
        self.changeInputBtn.setObjectName("changeInputBtn")
        self.generateBtn = QtWidgets.QPushButton(self.centralwidget)
        self.generateBtn.setGeometry(QtCore.QRect(365, 65, 100, 40))
        self.generateBtn.setStyleSheet("color: rgb(255, 255, 255);\n"
                                       "background-color: rgb(13, 246, 227);\n"
                                       "color: rgb(0, 0, 0);\n"
                                       "font: 75 italic 10pt \"Noto Sans\";\n"
                                       "border: 1px solid rgb(31, 26, 48);\n"
                                       "border-radius: 10px;")
        self.generateBtn.setObjectName("generateBtn")
        self.changeOutputBtn = QtWidgets.QPushButton(self.centralwidget)
        self.changeOutputBtn.setGeometry(QtCore.QRect(365, 205, 100, 40))
        self.changeOutputBtn.setStyleSheet("color: rgb(255, 255, 255);\n"
                                           "background-color: rgb(13, 246, 227);\n"
                                           "color: rgb(0, 0, 0);\n"
                                           "font: 75 italic 10pt \"Noto Sans\";\n"
                                           "border: 1px solid rgb(31, 26, 48);\n"
                                           "border-radius: 10px;")
        self.changeOutputBtn.setObjectName("changeOutputBtn")
        self.sortBtn = QtWidgets.QPushButton(self.centralwidget)
        self.sortBtn.setGeometry(QtCore.QRect(365, 275, 100, 40))
        self.sortBtn.setStyleSheet("color: rgb(255, 255, 255);\n"
                                   "background-color: rgb(13, 246, 227);\n"
                                   "color: rgb(0, 0, 0);\n"
                                   "font: 75 italic 10pt \"Noto Sans\";\n"
                                   "border: 1px solid rgb(31, 26, 48);\n"
                                   "border-radius: 10px;")
        self.sortBtn.setObjectName("sortBtn")
        self.firstComment = QtWidgets.QLabel(self.centralwidget)
        self.firstComment.setGeometry(QtCore.QRect(26, 110, 140, 20))
        self.firstComment.setStyleSheet("color: rgb(255, 255, 255);\n"
                                        "font: 75 11pt \"MS Shell Dlg 2\";")
        self.firstComment.setObjectName("firstComment")
        self.secondComment = QtWidgets.QLabel(self.centralwidget)
        self.secondComment.setGeometry(QtCore.QRect(26, 40, 140, 20))
        self.secondComment.setStyleSheet("color: rgb(255, 255, 255);\n"
                                         "font: 75 11pt \"MS Shell Dlg 2\";")
        self.secondComment.setObjectName("secondComment")
        self.thirdComment = QtWidgets.QLabel(self.centralwidget)
        self.thirdComment.setGeometry(QtCore.QRect(26, 180, 120, 20))
        self.thirdComment.setStyleSheet("color: rgb(255, 255, 255);\n"
                                        "font: 75 11pt \"MS Shell Dlg 2\";")
        self.thirdComment.setObjectName("secondComment_2")
        self.fourthComment = QtWidgets.QLabel(self.centralwidget)
        self.fourthComment.setGeometry(QtCore.QRect(26, 250, 120, 20))
        self.fourthComment.setStyleSheet("color: rgb(255, 255, 255);\n"
                                         "font: 75 11pt \"MS Shell Dlg 2\";")
        self.fourthComment.setObjectName("secondComment_3")
        ArraySorter.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(ArraySorter)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 385, 21))
        self.menubar.setObjectName("menubar")
        ArraySorter.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(ArraySorter)
        self.statusbar.setObjectName("statusbar")
        ArraySorter.setStatusBar(self.statusbar)

        self.retranslateUi(ArraySorter)
        QtCore.QMetaObject.connectSlotsByName(ArraySorter)

        self.updatePathLabels()

        self.addFunc()

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.title.setText(_translate("MainWindow", "Bubble array sorter"))
        self.changeInputBtn.setText(_translate("MainWindow", "Change Input"))
        self.generateBtn.setText(_translate("MainWindow", "Generate Array"))
        self.changeOutputBtn.setText(_translate("MainWindow", "Change Output"))
        self.sortBtn.setText(_translate("MainWindow", "Sort"))
        self.firstComment.setText(_translate("MainWindow", "Input Path:"))
        self.secondComment.setText(_translate("MainWindow", "Enter array size:"))
        self.thirdComment.setText(_translate("MainWindow", "Output Path:"))
        self.fourthComment.setText(_translate("MainWindow", "Sort Info:"))

    def addFunc(self):
        self.generateBtn.clicked.connect(self.generateArray)
        self.changeInputBtn.clicked.connect(self.chooseInputFile)
        self.changeOutputBtn.clicked.connect(self.chooseOutputFile)
        self.sortBtn.clicked.connect(self.sortArray)

    def chooseInputFile(self):
        fileName = self.askOpenFile(self.readFile)
        if fileName:
            self.readFile = fileName
        self.updatePathLabels()

    def chooseOutputFile(self):
        fileName = self.askWriteFile(self.writeFile)
        if fileName:
            self.writeFile = fileName
        self.updatePathLabels()

    def generateArray(self):
        userInput = self.lineEdit.text()
        fileName = self.askWriteFile(self.readFile)
        if not isValidInput(userInput) or not fileName:
            return

        arraySize = int(userInput)
        self.readFile = fileName
        self.updatePathLabels()

        arrayInit(fileName, arraySize)

    def sortArray(self):
        if not os.path.isfile(self.readFile):
            return
        array = readFile(self.readFile)
        arraySize = len(array)
        self.lineEdit.setText(str(arraySize))
        time = measureTime(sortArray, array)
        self.sortTimeLabel.setText("%d ms" % time)
        writeFile(self.writeFile, array)

    def askOpenFile(self, defaultPath):
        file_filter = 'Data File (*.csv);; All files (*)'
        pickedFile, _ = QFileDialog.getOpenFileName(
            parent=self,
            caption='Выберите название файла для сортировки',
            directory=defaultPath,
            filter=file_filter
        )
        return pickedFile if os.path.isfile(pickedFile) else None

    def askWriteFile(self, defaultPath):
        file_filter = 'Data File (*.csv);; All files (*)'
        pickedFile, _ = QFileDialog.getSaveFileName(
            parent=self,
            caption='Выберите название файла для результата',
            directory=defaultPath,
            filter=file_filter
        )
        return pickedFile

    def updatePathLabels(self):
        self.inputPathLabel.setText(self.readFile)
        self.outputPathLabel.setText(self.writeFile)
