import sys

from PyQt5.QtWidgets import QApplication, QMainWindow

from ui import *

if __name__ == "__main__":
    app = QApplication(sys.argv)
    ArraySorter = QMainWindow()
    ui = Ui_ArraySorter()
    ui.setupUi(ArraySorter)
    ArraySorter.show()
    sys.exit(app.exec_())
